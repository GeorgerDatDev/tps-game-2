#include "TpsGameCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Materials/Material.h"
#include "Engine/World.h"
#include <Kismet/GameplayStatics.h>
#include <Kismet/KismetMathLibrary.h>
#include "../FuncLib/Types.h"
#include "../TpsGameInstance.h"
#include "../TpsGame.h"
#include <Net/UnrealNetwork.h>

ATpsGameCharacter::ATpsGameCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->RotationRate = FRotator(0.f, 480.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	InventoryComponent = CreateDefaultSubobject<UInventoryBase>(TEXT("Inventory"));

	StatComponent = CreateDefaultSubobject<UTpsPlayerStatSystem>(TEXT("Stats"));
	StatComponent->OwningCharacter = this;

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

}

void ATpsGameCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

	MovementTick(DeltaSeconds);

	if (GetCurrentWeapon()->IsValidLowLevel())
	{
		GetCurrentWeapon()->FireTick(DeltaSeconds);
		GetCurrentWeapon()->ReloadTick(DeltaSeconds);
	
	}

	if (!IsCurrentState(EPlayerState::Dead_State))
	{
		StatComponent->RegenerateHealth(DeltaSeconds);

	}

}

void ATpsGameCharacter::BeginPlay()
{
	Super::BeginPlay();

	if (GetInventoryComponent()->WeaponSlots.IsValidIndex(0))
	{
		InitWeapon(GetInventoryComponent()->WeaponSlots[0].WeaponName);
		GetInventoryComponent()->EquippedWeaponName = GetInventoryComponent()->WeaponSlots[0].WeaponName;
		GetInventoryComponent()->IdentifyAsEquipped(0);
		GetInventoryComponent()->OnSlotChange.Broadcast();

	}

}

void ATpsGameCharacter::SetupPlayerInputComponent(UInputComponent* InputComp)
{
	Super::SetupPlayerInputComponent(InputComp);

	InputComp->BindAxis(TEXT("MoveForward"), this, &ATpsGameCharacter::InputAxisX);
	InputComp->BindAxis(TEXT("MoveRight"), this, &ATpsGameCharacter::InputAxisY);

	InputComp->BindAction(TEXT("FireAction"), EInputEvent::IE_Pressed, this, &ATpsGameCharacter::InputFirePressed);
	InputComp->BindAction(TEXT("FireAction"), EInputEvent::IE_Released, this, &ATpsGameCharacter::InputFireReleased);

	InputComp->BindAction(TEXT("ReloadAction"), EInputEvent::IE_Pressed, this, &ATpsGameCharacter::OnDemandReload);

	InputComp->BindAction(TEXT("SwitchSlotOne"), EInputEvent::IE_Pressed, this, &ATpsGameCharacter::InputSwitchWeapon<0>);
	InputComp->BindAction(TEXT("SwitchSlotTwo"), EInputEvent::IE_Pressed, this, &ATpsGameCharacter::InputSwitchWeapon<1>);
	InputComp->BindAction(TEXT("SwitchSlotThree"), EInputEvent::IE_Pressed, this, &ATpsGameCharacter::InputSwitchWeapon<2>);
	InputComp->BindAction(TEXT("SwitchSlotFour"), EInputEvent::IE_Pressed, this, &ATpsGameCharacter::InputSwitchWeapon<3>);
	InputComp->BindAction(TEXT("SwitchSlotFive"), EInputEvent::IE_Pressed, this, &ATpsGameCharacter::InputSwitchWeapon<4>);
	InputComp->BindAction(TEXT("SwitchSlotSix"), EInputEvent::IE_Pressed, this, &ATpsGameCharacter::InputSwitchWeapon<5>);
	InputComp->BindAction(TEXT("SwitchSlotSeven"), EInputEvent::IE_Pressed, this, &ATpsGameCharacter::InputSwitchWeapon<6>);
	InputComp->BindAction(TEXT("SwitchSlotEight"), EInputEvent::IE_Pressed, this, &ATpsGameCharacter::InputSwitchWeapon<7>);
	InputComp->BindAction(TEXT("SwitchSlotNine"), EInputEvent::IE_Pressed, this, &ATpsGameCharacter::InputSwitchWeapon<8>);
	InputComp->BindAction(TEXT("SwitchSlotTen"), EInputEvent::IE_Pressed, this, &ATpsGameCharacter::InputSwitchWeapon<9>);

}

void ATpsGameCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ATpsGameCharacter, CurrentState);

}

UInventoryBase* ATpsGameCharacter::GetInventoryComponent()
{
	return InventoryComponent;

}

UTpsPlayerStatSystem* ATpsGameCharacter::GetStatComponent()
{
	return StatComponent;
}

void ATpsGameCharacter::InputAxisX(float Value)
{
	AxisX = Value;

}

void ATpsGameCharacter::InputAxisY(float Value)
{
	AxisY = Value;

}

void ATpsGameCharacter::MovementTick(float DeltaTime)
{
	if (Controller && !Controller->IsLocalPlayerController())
	{
		if (!IsCurrentState(EPlayerState::Immobilized_State) && !IsCurrentState(EPlayerState::Dead_State))
		{
			AddMovementInput(FVector(1.0f, 0.0f, 0.0f), AxisX);
			AddMovementInput(FVector(0.0f, 1.0f, 0.0f), AxisY);

			if (!IsCurrentState(EPlayerState::Sprint_State))
			{
				APlayerController* ShooterController = UGameplayStatics::GetPlayerController(GetWorld(), 0);

				if (ShooterController)
				{
					FHitResult ResultOfHit;
					ShooterController->GetHitResultUnderCursor(ECC_GameTraceChannel1, false, ResultOfHit);

					FRotator NewRotation = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), ResultOfHit.Location);

					SetRotationYaw_Server(NewRotation.Yaw);

				}
				if (StatComponent->GetCurrentStamina() < StatComponent->GetMaxStamina())
				{
					StatComponent->UseStamina((-7 * DeltaTime) / (1 + (GetVelocity().Size() * 0.005)));
				}

			}
			else if (StatComponent->GetCurrentStamina() > 0)
			{
				if (AxisX != 0)
				{
					StatComponent->UseStamina(5 * DeltaTime);
				}
				else if (AxisY != 0)
				{
					StatComponent->UseStamina(5 * DeltaTime);
				}

			}

		}

	}
}

void ATpsGameCharacter::CharacterUpdate()
{
	float rSpeed = 400.0f;

	switch (CurrentState)
	{
	case EPlayerState::Walk_State:
		rSpeed = MovementStats.Walk_Speed * StatComponent->GetMovementSpeed() * PlayerSpeedMultiplier;
		GetCharacterMovement()->bOrientRotationToMovement = false;
		break;

	case EPlayerState::Sprint_State:
		rSpeed = MovementStats.Sprint_Speed * StatComponent->GetMovementSpeed() * PlayerSpeedMultiplier;
		GetCharacterMovement()->bOrientRotationToMovement = true;
		break;

	case EPlayerState::Aim_State:
		rSpeed = MovementStats.Aim_Speed * StatComponent->GetMovementSpeed() * PlayerSpeedMultiplier;
		break;

	default:
		break;

	}

	GetCharacterMovement()->MaxWalkSpeed = rSpeed;

}

bool ATpsGameCharacter::IsCurrentState(EPlayerState DisState)
{
	return CurrentState == DisState;

}

void ATpsGameCharacter::SetPlayerState(EPlayerState NewState)
{
	SetPlayerState_Server(NewState);

}

AWeaponBase* ATpsGameCharacter::GetCurrentWeapon()
{
	return CurrentWeapon;

}

void ATpsGameCharacter::SwitchWeapon(int32 Index)
{
	if (GetInventoryComponent()->WeaponSlots.IsValidIndex(Index))
	{
		if (GetInventoryComponent()->WeaponSlots[Index].WeaponName != GetInventoryComponent()->EquippedWeaponName && GetInventoryComponent()->IsWeaponNameValid(GetInventoryComponent()->WeaponSlots[Index].WeaponName))
		{
			GetInventoryComponent()->SetExtraWeaponProperties(GetInventoryComponent()->GetEquippedWeaponIndex(), CurrentWeapon->ExtraSettings);

			CurrentWeapon->Destroy();

			InitWeapon(GetInventoryComponent()->WeaponSlots[Index].WeaponName);
			if (GetInventoryComponent()->WeaponSlots[Index].HasBeenEquipped)
			{
				CurrentWeapon->ExtraSettings = GetInventoryComponent()->WeaponSlots[Index].ExtraProperties;

			}
			else
			{
				GetInventoryComponent()->IdentifyAsEquipped(Index);

			}

			GetInventoryComponent()->EquippedWeaponName = GetInventoryComponent()->WeaponSlots[Index].WeaponName;
			StopAnimMontage(CurrentWeapon->WeaponSettings.ReloadAnim);
			
			GetInventoryComponent()->OnSlotChange.Broadcast();

		}
		
	}

}

void ATpsGameCharacter::InitWeapon(FName WeaponID)
{
	UTpsGameInstance* DisInstance = Cast<UTpsGameInstance>(GetGameInstance());
	FWeaponProperties NewProperties;

	if (DisInstance)
	{
		if (DisInstance->GetWeaponByName(WeaponID, NewProperties))
		{
			if (NewProperties.WeaponClass)
			{
				FVector SpawnOffset = FVector(0);
				FRotator SpawnRotation = FRotator(0);

				FActorSpawnParameters SpawnParam;
				SpawnParam.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParam.Owner = GetOwner();
				SpawnParam.Instigator = GetInstigator();

				CurrentWeapon = Cast<AWeaponBase>(GetWorld()->SpawnActor(NewProperties.WeaponClass, &SpawnOffset, &SpawnRotation, SpawnParam));

				if (CurrentWeapon)
				{
					CurrentWeapon->OwningCharacter = this;
					CurrentWeapon->WeaponSettings = NewProperties;

					FAttachmentTransformRules AttachRules(EAttachmentRule::SnapToTarget, false);
					CurrentWeapon->AttachToComponent(GetMesh(), AttachRules, FName("Weapon"));
					CurrentWeapon->ExtraSettings.CurrentAmmo = CurrentWeapon->WeaponSettings.MaxAmmo;

				}

			}
			else
			{
				UE_LOG(LogTemp, Warning, TEXT("ATpsGameCharacter::InitWeapon - Initialization Failed ( WeaponClass = NULL )"));
			}
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("ATpsGameCharacter::InitWeapon - Initialization Failed ( Invalid WeaponID || WeaponID = NULL )"));
		}
	}

}

void ATpsGameCharacter::EventFire(bool bFiring)
{
	AWeaponBase* DisGun = nullptr;
	DisGun = GetCurrentWeapon();

	if (DisGun)
	{
		DisGun->SetIsFiring(bFiring);

	}

}

void ATpsGameCharacter::InputFirePressed()
{
	EventFire(true);

}

void ATpsGameCharacter::InputFireReleased()
{
	EventFire(false);

}

void ATpsGameCharacter::OnDemandReload()
{
	if (GetCurrentWeapon()->ExtraSettings.CurrentAmmo < GetCurrentWeapon()->WeaponSettings.MaxAmmo)
	{
		GetCurrentWeapon()->StartReload();

	}

}

void ATpsGameCharacter::Consume(EConsumableType Consumable)
{
	bool Found = false;
	bool Done = false;

	for (int32 i = 0; i < InventoryComponent->ConsumableSlots.Num(); i++)
	{
		if (InventoryComponent->ConsumableSlots[i].Type == Consumable)
		{
			if (InventoryComponent->ConsumableSlots[i].Count > 0)
			{
				Found = true;

				switch (Consumable)
				{
				case EConsumableType::Healing:

					if (StatComponent->GetCurrentHealth() < StatComponent->GetRegenThreshold())
					{
						StatComponent->IncreaseRegeneration(5.0f, 25);
						Done = true;

					}
					else
					{
						OnItemConsumtionFailed.Broadcast(FText::FromString("Health Above Threshold"));

					}
					break;

				case EConsumableType::InstHealing:

					if (StatComponent->GetCurrentHealth() < StatComponent->GetMaxHealth())
					{
						StatComponent->Heal(50.0f);
						Done = true;

					}
					else
					{
						OnItemConsumtionFailed.Broadcast(FText::FromString("Health Full"));

					}
					break;

				case EConsumableType::SpeedBoost:

					Done = UseStim();
					CharacterUpdate();
					break;

				case EConsumableType::ArmorRefill:

					Done = StatComponent->AddArmor();
					break;

				default:

					break;

				}

				if (Done)
				{
					InventoryComponent->ConsumableSlots[i].Count--;
					InventoryComponent->OnConsumableUpdate.Broadcast();
					UGameplayStatics::SpawnSoundAttached(InventoryComponent->ConsumableSlots[i].SoundOnConsumption, RootComponent);
					return;

				}

			}

		}

	}

	if (!Found)
	{
		OnItemConsumtionFailed.Broadcast(FText::FromString("Consumable not Found"));

	}

}

void ATpsGameCharacter::SetRotationYaw_Server_Implementation(float Yaw)
{
	SetRotationYaw_Multi(Yaw);

}

void ATpsGameCharacter::SetRotationYaw_Multi_Implementation(float Yaw)
{
	if (Controller && !Controller->IsLocalPlayerController())
	{
		SetActorRotation(FRotator(GetActorRotation().Pitch, Yaw, GetActorRotation().Roll));

	}

}

void ATpsGameCharacter::SetPlayerState_Server_Implementation(EPlayerState NewState)
{
	SetPlayerState_Multi(NewState);

}

void ATpsGameCharacter::SetPlayerState_Multi_Implementation(EPlayerState NewState)
{
	CurrentState = NewState;
	CharacterUpdate();

}

bool ATpsGameCharacter::UseStim()
{
	if (PlayerSpeedMultiplier == 1.0f)
	{
		FTimerHandle StimTimer;
		PlayerSpeedMultiplier *= 1.2f;
		StatComponent->TakeDamage((StatComponent->GetCurrentHealth() * 0.1), true);

		GetWorld()->GetTimerManager().SetTimer(StimTimer, this, &ATpsGameCharacter::ResetSpeedMultiplier, 10.0f, false);

		return true;

	}

	OnItemConsumtionFailed.Broadcast(UKismetMathLibrary::RandomBoolWithWeight(0.2) ? FText::FromString("Nope, Heart Attack") : FText::FromString("Stim Already in Use"));
	return false;

}

void ATpsGameCharacter::ResetSpeedMultiplier()
{
	PlayerSpeedMultiplier = 1.0f;

}

