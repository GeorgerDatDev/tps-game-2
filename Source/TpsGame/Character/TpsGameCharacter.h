#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "../FuncLib/Types.h"
#include "../WeaponBase.h"
#include "../InventoryBase.h"
#include "../TpsHealthSystem.h"
#include <TpsGame/TpsPlayerStatSystem.h>
#include "TpsGameCharacter.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnMessageSent, FText, Message);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnRandomDelegate);

UCLASS(Blueprintable)
class ATpsGameCharacter : public ACharacter
{
	GENERATED_BODY()

public:

	ATpsGameCharacter();

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	virtual void BeginPlay() override;

	virtual void SetupPlayerInputComponent(UInputComponent* InputComp) override;

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	UPROPERTY(BlueprintAssignable)
	FOnMessageSent OnPickupReady;
	UPROPERTY(BlueprintAssignable)
	FOnMessageSent OnItemConsumtionFailed;
	UPROPERTY(BlueprintAssignable)
	FOnRandomDelegate OnCharDeath;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }

private:

	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

protected:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Components)
		UInventoryBase* InventoryComponent;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Components)
		UTpsPlayerStatSystem* StatComponent;

public:

	UFUNCTION()
	UInventoryBase* GetInventoryComponent();
	UFUNCTION()
	UTpsPlayerStatSystem* GetStatComponent();

	UPROPERTY()
	float PlayerSpeedMultiplier = 1.0f;

	UPROPERTY(Replicated, BlueprintReadWrite, Category = "Movement")
		EPlayerState CurrentState;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Movement");
		FMovementSpeed MovementStats;

	//

		AWeaponBase* CurrentWeapon;

	//

	UFUNCTION()
		void InputAxisX(float Value);
	UFUNCTION()
		void InputAxisY(float Value);

	float AxisX, AxisY = 0.0f;

	void MovementTick(float DeltaTime);

	//

	UFUNCTION(BlueprintCallable)
		void CharacterUpdate();
	UFUNCTION(BlueprintCallable)
		void SetPlayerState(EPlayerState NewState);

	UFUNCTION(BlueprintPure)
		bool IsCurrentState(EPlayerState DisState);

	//

	UFUNCTION(BlueprintPure)
		AWeaponBase* GetCurrentWeapon();

	UFUNCTION(BlueprintCallable)
		void SwitchWeapon(int32 Index);

	void InitWeapon(FName WeaponID);

	//

	UFUNCTION(BlueprintCallable)
		void EventFire(bool bFiring);

	UFUNCTION()
		void InputFirePressed();
	UFUNCTION()
		void InputFireReleased();

	UFUNCTION()
		void OnDemandReload();

	UFUNCTION()
	bool UseStim();

	void ResetSpeedMultiplier();

	UFUNCTION(BlueprintCallable)
		void Consume(EConsumableType Consumable);

	//

	template<int32 Index>
	void InputSwitchWeapon()
	{
		if (GetInventoryComponent()->SwitchWeaponToIndex(GetInventoryComponent()->GetEquippedWeaponIndex(), 0, GetCurrentWeapon()->ExtraSettings)) {}

	}

	//

	UFUNCTION(Server, Unreliable)
	void SetRotationYaw_Server(float Yaw);
	UFUNCTION(NetMulticast, Unreliable)
	void SetRotationYaw_Multi(float Yaw);

	UFUNCTION(Server, Reliable)
	void SetPlayerState_Server(EPlayerState NewState);
	UFUNCTION(NetMulticast, Reliable)
	void SetPlayerState_Multi(EPlayerState NewState);

};
