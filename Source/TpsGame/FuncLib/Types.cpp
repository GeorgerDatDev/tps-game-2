// Fill out your copyright notice in the Description page of Project Settings.


#include "Types.h"
#include "../TpsGame.h"

FName UTypes::GetAmmoNameFromType(EAmmoType InType)
{
	switch (InType)
	{
	case EAmmoType::LightRifle:

		return TEXT("5.56x40mm ammunition");

	case EAmmoType::HeavyRiflle:

		return TEXT("7.62x39mm ammunition");

	case EAmmoType::LightPistol:

		return TEXT("9mm ammunition");


	case EAmmoType::Shotgun:

		return TEXT("12 Gague ammunition");

	case EAmmoType::Grenade:

		return TEXT("Grenade(s)");

	default:

		break;

	}

	return TEXT("");

}

FName UTypes::GetAmmoPureNameFromType(EAmmoType InType)
{
	switch (InType)
	{
	case EAmmoType::LightRifle:

		return TEXT("RifleLight");

	case EAmmoType::HeavyRiflle:

		return TEXT("RifleHeavy");

	case EAmmoType::LightPistol:

		return TEXT("PistolLight");


	case EAmmoType::Shotgun:

		return TEXT("Shotgun");

	case EAmmoType::Grenade:

		return TEXT("Grenade");

	default:

		break;

	}

	return TEXT("");

}
