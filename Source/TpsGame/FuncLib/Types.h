#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "Engine/DataTable.h"
#include "Types.generated.h"

UENUM(BlueprintType)
enum class EPlayerState : uint8
{
	Walk_State UMETA(DisplayName = "Walk"),
	Sprint_State UMETA(DisplayName = "Sprint"),
	Aim_State UMETA(DisplayName = "Aim"),
	Immobilized_State UMETA(DisplayName = "Immobilized"),
	Dead_State UMETA(DisplayName = "Dead")

};

UENUM(BlueprintType)
enum class EAmmoType : uint8
{
	LightRifle UMETA(DisplayName = "5.56x40mm"),
	HeavyRiflle UMETA(DisplayName = "7.62.39mm"),
	Shotgun UMETA(DisplayName = "12 Gague"),
	Grenade UMETA(DisplayName = "Grenade"),
	LightPistol UMETA(DisplayName = "9mm")

};

UENUM(BlueprintType)
enum class EConsumableType : uint8
{
	Healing UMETA(DisplayName = "First Aid Kit"),
	InstHealing UMETA(DisplayName = "Proffesional Med Kit"),
	SpeedBoost UMETA(DisplayName = "Stim"),
	ArmorRefill UMETA(DisplayName = "Armor Plate")

};

USTRUCT(BlueprintType)
struct FMovementSpeed
{
	GENERATED_BODY()

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Movement")
		float Walk_Speed = 300.0f;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Movement")
		float Sprint_Speed = 600.0f;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Movement")
		float Aim_Speed = 180.0f;
};

USTRUCT(BlueprintType)
struct FProjectileProperties
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile Settings")
		TSubclassOf<class AProjectileBase> Projectile = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Projectile Settings")
		float Damage = 5.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile Settings")
		TSubclassOf<UDamageType> ProjectileDamageType = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile Settings")
		float Lifetime = 2.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile Settings")
		float Velocity = 3000.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile Settings")
		int32 PiercingLvl = 0;

	//

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Effects")
		UParticleSystem* Particle = nullptr;
	/**Particle to play when in contact with a character/flesh*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Effects")
		UParticleSystem* CharacterParticle = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Effects")
		USoundBase* Sound = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Effects")
		UMaterialInterface* Decal = nullptr;
	
	//

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Explosive Settings")
		bool IsExplosive;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Explosive Settings")
		float MaxExplDmg = 150.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Explosive Settings")
		float MaxExplRad = 500.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Explosive Settings")
		float DetonationTime = 3.0f;

};

USTRUCT(BlueprintType)
struct FWeaponDispersion
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadOnly)
		float CurrentDispersion = 0.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
		float InitialDispersion = 5.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
		float AimMultiplier = 3.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
		float MovementCoef = 1.0f;

};

USTRUCT(BlueprintType)
struct FWeaponProperties : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Class")
		TSubclassOf<class AWeaponBase> WeaponClass = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Class")
		UStaticMesh* DroppedMesh;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Class")
		EAmmoType Caliber;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Class")
		UTexture* Sprite;

	//

	/**The fire rate of the weapon (rounds/second)*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
		float FireRate = 2.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
		float ReloadTime = 1.5f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
		int32 MaxAmmo = 15;

	//

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
		FWeaponDispersion Dispersion;

	//

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sound")
		USoundBase* FireSound = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sound")
		USoundBase* ReloadSoundEmpty = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sound")
		USoundBase* ReloadSoundTactical = nullptr;

	//

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visual")
		UParticleSystem* FireEffect = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visual")
		TSubclassOf<AActor> BulletShell = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visual")
		UAnimMontage* ReloadAnim = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visual")
		float AnimEmptySpeed = 1.5f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visual")
		float AnimTacticalSpeed = 1.4f;

	//

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile")
		FProjectileProperties ProjectileSettings;

	//

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Trace")
		float TraceDamage = 20.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Trace")
		float TraceDistance = 500.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Trace")
		UParticleSystem* TraceEffect = nullptr;

};

USTRUCT(BlueprintType)
struct FExtraProperties
{
	GENERATED_BODY()

public:

	UPROPERTY(BlueprintReadOnly)
		int32 CurrentAmmo = 0;
	UPROPERTY()
		bool IsReloading = false;
	UPROPERTY(BlueprintReadOnly)
		float ReloadDelta = 0.0f;
	
};

USTRUCT(BlueprintType)
struct FAmmoProperties : public FTableRowBase
{
	GENERATED_BODY()

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visual")
	UTexture* Sprite = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Visual")
	FVector BoxColor;

};

USTRUCT(BlueprintType)
struct FWeaponSlot
{
	GENERATED_BODY()

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponSlot")
		FName WeaponName;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponSlot")
		FExtraProperties ExtraProperties;
	UPROPERTY(BlueprintReadWrite, Category = "WeaponSlot")
		bool HasBeenEquipped = false;

};

USTRUCT(BlueprintType)
struct FAmmoSlot
{
	GENERATED_BODY()

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AmmoSlot")
		EAmmoType Type = EAmmoType::LightRifle;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AmmoSlot")
		int32 Count = 60;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AmmoSlot")
		int32 Capacity = 120;

};

USTRUCT(BlueprintType)
struct FConsumableSlot
{
	GENERATED_BODY()

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AmmoSlot")
	EConsumableType Type = EConsumableType::Healing;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AmmoSlot")
	int32 Count = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AmmoSlot")
	int32 Capacity = 4;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AmmoSlot")
	USoundBase* SoundOnConsumption = nullptr;

};

UCLASS()
class TPSGAME_API UTypes : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:

	UFUNCTION(BlueprintPure)
	static FName GetAmmoNameFromType(EAmmoType InType);

	UFUNCTION(BlueprintPure)
	static FName GetAmmoPureNameFromType(EAmmoType InType);

};
