// Copyright Epic Games, Inc. All Rights Reserved.

#include "TpsGameGameMode.h"
#include "TpsGamePlayerController.h"
#include "../Character/TpsGameCharacter.h"
#include "UObject/ConstructorHelpers.h"

ATpsGameGameMode::ATpsGameGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ATpsGamePlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprints/Character/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
