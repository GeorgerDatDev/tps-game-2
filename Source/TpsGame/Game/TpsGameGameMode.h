// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TpsGameGameMode.generated.h"

UCLASS(minimalapi)
class ATpsGameGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ATpsGameGameMode();
};



