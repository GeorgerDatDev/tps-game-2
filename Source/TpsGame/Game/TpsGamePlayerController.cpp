// Copyright Epic Games, Inc. All Rights Reserved.

#include "TpsGamePlayerController.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"
#include "Runtime/Engine/Classes/Components/DecalComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "../Character/TpsGameCharacter.h"
#include "Engine/World.h"
#include <Kismet/KismetMathLibrary.h>

ATpsGamePlayerController::ATpsGamePlayerController()
{
	bShowMouseCursor = true;
	DefaultMouseCursor = EMouseCursor::Crosshairs;

	PlayerCharacter = GetPawn<ATpsGameCharacter>();

}

void ATpsGamePlayerController::PlayerTick(float DeltaTime)
{
	Super::PlayerTick(DeltaTime);

	

}

void ATpsGamePlayerController::SetupInputComponent()
{
	// set up gameplay key bindings
	Super::SetupInputComponent();

	

}

