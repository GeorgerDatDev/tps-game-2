// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "../Character/TpsGameCharacter.h"
#include "TpsGamePlayerController.generated.h"

UCLASS()
class ATpsGamePlayerController : public APlayerController
{
	GENERATED_BODY()

	ATpsGameCharacter* PlayerCharacter = nullptr;

public:
	ATpsGamePlayerController();

protected:

	virtual void PlayerTick(float DeltaTime) override;
	virtual void SetupInputComponent() override;
	
};


