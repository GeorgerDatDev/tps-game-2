#include "InventoryBase.h"
#include "TpsGameInstance.h"
#include <Kismet/GameplayStatics.h>

// Sets default values for this component's properties
UInventoryBase::UInventoryBase()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	DisInstance = Cast<UTpsGameInstance>(UGameplayStatics::GetGameInstance(GetWorld()));

	// ...
}


// Called when the game starts
void UInventoryBase::BeginPlay()
{
	Super::BeginPlay();
	
}


// Called every frame
void UInventoryBase::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

bool UInventoryBase::SwitchWeaponToIndex(int32 Current, int32 Target, FExtraProperties OldProperties)
{
	bool Success = false;

	FWeaponSlot BackupCurrent;
	FWeaponSlot BackupTarget;

	if (Current != Target)
	{
		if (WeaponSlots.IsValidIndex(Current) && WeaponSlots.IsValidIndex(Target))
		{
			SetExtraWeaponProperties(Current, OldProperties);

			BackupCurrent = WeaponSlots[Current];
			BackupTarget = WeaponSlots[Target];
			WeaponSlots[Current] = BackupTarget;
			WeaponSlots[Target] = BackupCurrent;

			OnSlotChange.Broadcast();

			if (WeaponSlots[Current].WeaponName == BackupTarget.WeaponName && WeaponSlots[Target].WeaponName == BackupCurrent.WeaponName)
			{
				return true;

			}

		}

	}

	return false;

}

FExtraProperties UInventoryBase::GetExtraWeaponProperties(int32 Index)
{
	return WeaponSlots[Index].ExtraProperties;

}

int32 UInventoryBase::GetWeaponIndexByName(FName WeaponID)
{
	for (int32 i = 0; i < WeaponSlots.Num(); i++)
	{
		if (WeaponSlots[i].WeaponName == WeaponID)
		{
			return i;

		}

	}

	UE_LOG(LogTemp, Warning, TEXT("UInventoryBase::GetWeaponIndexByName - Weapon not found ( WeaponID is null or is not contained within the array )"));

	return {};

}

bool UInventoryBase::IsWeaponNameValid(FName WeaponID)
{
	for (int32 i = 0; i < WeaponSlots.Num(); i++)
	{
		if (DisInstance->GetWeaponByName(WeaponID, UselessProperties))
		{
			return true;

		}

	}

	return false;

}

bool UInventoryBase::IsWeaponInInventory(FName InName)
{
	for (int32 i = 0; i < WeaponSlots.Num(); i++)
	{
		if (WeaponSlots[i].WeaponName == InName)
		{
			return true;

		}

	}

	return false;

}

void UInventoryBase::SetExtraWeaponProperties(int32 Index, FExtraProperties InProperties)
{
	WeaponSlots[Index].ExtraProperties = InProperties;

}

FWeaponSlot UInventoryBase::GetEquippedWeapon()
{
	return WeaponSlots[GetWeaponIndexByName(EquippedWeaponName)];

}

int32 UInventoryBase::GetEquippedWeaponIndex()
{
	return GetWeaponIndexByName(EquippedWeaponName);

}

void UInventoryBase::IdentifyAsEquipped(int32 Index)
{
	WeaponSlots[Index].HasBeenEquipped = true;

}

void UInventoryBase::UpdateProperties(AWeaponBase* Weapon, int32 Index)
{
	WeaponSlots[Index].ExtraProperties = Weapon->ExtraSettings;
	OnSlotChange.Broadcast();

}

FAmmoSlot UInventoryBase::GetAmmoSlotByType(EAmmoType Type)
{
	for (int32 i = 0; i < AmmoSlots.Num(); i++)
	{
		if (AmmoSlots[i].Type == Type)
		{
			return AmmoSlots[i];

		}

	}

	UE_LOG(LogTemp, Warning, TEXT("UInventoryBase::GetAmmoSlotByType - Ammunition not found ( Type is not contained within the array )"));

	return {};
}

int32 UInventoryBase::GetAmmoIndexByType(EAmmoType Type)
{
	for (int32 i = 0; i < AmmoSlots.Num(); i++)
	{
		if (AmmoSlots[i].Type == Type)
		{
			return i;

		}

	}

	UE_LOG(LogTemp, Warning, TEXT("UInventoryBase::GetAmmoIndexByType - Ammunition not found ( Type is not contained within the array )"));

	return {};
}

int32 UInventoryBase::UseAmmo(EAmmoType Type, int32 Amount, int32 WeaponAmmo)
{
	int32 NewAmmo = Amount;

	AmmoSlots[GetAmmoIndexByType(Type)].Count -= (Amount - WeaponAmmo);

	if (AmmoSlots[GetAmmoIndexByType(Type)].Count < 0)
	{
		NewAmmo += AmmoSlots[GetAmmoIndexByType(Type)].Count;
		AmmoSlots[GetAmmoIndexByType(Type)].Count = 0;

	}

	return NewAmmo;

}

int32 UInventoryBase::AddAmmoToInventory(EAmmoType Type, int32 Amount)
{
	int32 OutValue = 0;

	if (AmmoSlots[GetAmmoIndexByType(Type)].Count + Amount > AmmoSlots[GetAmmoIndexByType(Type)].Capacity)
	{
		AmmoSlots[GetAmmoIndexByType(Type)].Count = AmmoSlots[GetAmmoIndexByType(Type)].Capacity;
		OutValue = AmmoSlots[GetAmmoIndexByType(Type)].Count + Amount - AmmoSlots[GetAmmoIndexByType(Type)].Capacity;

	}
	else
	{
		AmmoSlots[GetAmmoIndexByType(Type)].Count += Amount;

	}

	OnWeaponReload.Broadcast();
	return OutValue;

}

int32 UInventoryBase::GetFirstValidWeapon()
{
	for (int32 i = 0; i < WeaponSlots.Num(); i++)
	{
		if (DisInstance->GetWeaponByName(WeaponSlots[i].WeaponName, UselessProperties))
		{
			return i;

		}

	}

	return {};

}

bool UInventoryBase::CheckForUnequippedWeapons()
{
	for (int32 i = 0; i < WeaponSlots.Num(); i++)
	{
		if (DisInstance->GetWeaponByName(WeaponSlots[i].WeaponName, UselessProperties) && WeaponSlots[i].WeaponName != EquippedWeaponName)
		{
			return true;

		}

	}

	return false;
}

int32 UInventoryBase::GetFirstValidWeaponWithAmmo()
{
	for (int32 i = 0; i < WeaponSlots.Num(); i++)
	{
		if (DisInstance->GetWeaponByName(WeaponSlots[i].WeaponName, UselessProperties) && WeaponSlots[i].ExtraProperties.CurrentAmmo > 0)
		{
			return i;

		}

	}

	return {};

}

bool UInventoryBase::AddWeaponToInventory(FWeaponSlot Weapon, int32 &OutIndex)
{
	if (!IsWeaponInInventory(Weapon.WeaponName))
	{
		for (int32 i = 0; i < WeaponSlots.Num(); i++)
		{
			if (!DisInstance->GetWeaponByName(WeaponSlots[i].WeaponName, UselessProperties))
			{
				WeaponSlots[i] = Weapon;
				OnSlotChange.Broadcast();
				OutIndex = i;
				return true;

			}

		}

	}
	else
	{
		OnPickupFailed.Broadcast();

	}

	return false;

}

void UInventoryBase::RemoveWeaponFromInventory(int32 Index)
{
	if (IsWeaponNameValid(WeaponSlots[Index].WeaponName))
	{
		WeaponSlots[Index] = FWeaponSlot();
		OnSlotChange.Broadcast();

	}

}

bool UInventoryBase::AddConsumableToInventory(EConsumableType Consumable)
{
	for (int32 i = 0; i < ConsumableSlots.Num(); i++)
	{
		if (ConsumableSlots[i].Type == Consumable)
		{
			if (ConsumableSlots[i].Count + 1 <= ConsumableSlots[i].Capacity)
			{
				ConsumableSlots[i].Count++;
				return true;

			}

		}

	}

	return false;

}

