#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "FuncLib/Types.h"
#include "WeaponBase.h"
#include "InventoryBase.generated.h"

class UTpsGameInstance;

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnUiUpdate);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TPSGAME_API UInventoryBase : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UInventoryBase();
	UPROPERTY(BlueprintAssignable)
	FOnUiUpdate OnWeaponReload;

	UPROPERTY(BlueprintAssignable)
	FOnUiUpdate OnSlotChange;
	
	UPROPERTY(BlueprintAssignable)
	FOnUiUpdate OnPickupFailed;

	UPROPERTY(BlueprintAssignable)
	FOnUiUpdate OnConsumableUpdate;

	UPROPERTY(BlueprintReadOnly)
		FName EquippedWeaponName;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UTpsGameInstance* DisInstance = nullptr;
	FWeaponProperties UselessProperties;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Slots")
	TArray<FWeaponSlot> WeaponSlots;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Slots")
	TArray<FAmmoSlot> AmmoSlots;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Slots")
	TArray<FConsumableSlot> ConsumableSlots;


	bool SwitchWeaponToIndex(int32 Current, int32 Target, FExtraProperties OldProperties);

	FExtraProperties GetExtraWeaponProperties(int32 Index);

	UFUNCTION(BlueprintPure)
		int32 GetWeaponIndexByName(FName WeaponID);

	bool IsWeaponNameValid(FName WeaponID);
	bool IsWeaponInInventory(FName InName);
	void SetExtraWeaponProperties(int32 Index, FExtraProperties InProperties);

	UFUNCTION(BlueprintPure)
		FWeaponSlot GetEquippedWeapon();

	UFUNCTION(BlueprintPure)
		int32 GetEquippedWeaponIndex();
	void IdentifyAsEquipped(int32 Index);
	void UpdateProperties(AWeaponBase* Weapon, int32 Index);

	UFUNCTION(BlueprintPure)
		FAmmoSlot GetAmmoSlotByType(EAmmoType Type);

	int32 GetAmmoIndexByType(EAmmoType Type);

	int32 UseAmmo(EAmmoType Type, int32 WeaponCurrentAmmo, int32 WeaponMaxAmmo);

	UFUNCTION(BlueprintCallable)
	UPARAM(DisplayName = "Ammo Left") int32 AddAmmoToInventory(EAmmoType Type, int32 Amount);

	UFUNCTION(BlueprintPure)
	int32 GetFirstValidWeapon();

	UFUNCTION(BlueprintPure)
	bool CheckForUnequippedWeapons();

	UFUNCTION(BlueprintPure)
	int32 GetFirstValidWeaponWithAmmo();

	UFUNCTION(BlueprintCallable)
	UPARAM(DisplayName = "Success") bool AddWeaponToInventory(FWeaponSlot Weapon, int32& OutIndex);

	UFUNCTION(BlueprintCallable)
	void RemoveWeaponFromInventory(int32 Index);

	UFUNCTION(BlueprintCallable)
	UPARAM(DisplayName = "Success") bool AddConsumableToInventory(EConsumableType Consumable);
		
};
