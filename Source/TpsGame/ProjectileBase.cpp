// Fill out your copyright notice in the Description page of Project Settings.


#include "ProjectileBase.h"
#include <Kismet/GameplayStatics.h>
#include <Kismet/KismetMathLibrary.h>

// Sets default values
AProjectileBase::AProjectileBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	CollisionSphere = CreateDefaultSubobject<USphereComponent>(TEXT("Collision Sphere"));
	CollisionSphere->SetSphereRadius(15.0f);

	CollisionSphere->OnComponentHit.AddDynamic(this, &AProjectileBase::CollisionSphereHit);
	CollisionSphere->OnComponentBeginOverlap.AddDynamic(this, &AProjectileBase::CollisionSphereBeginOverlap);
	CollisionSphere->OnComponentEndOverlap.AddDynamic(this, &AProjectileBase::CollisionSphereEndOverlap);

	CollisionSphere->bReturnMaterialOnMove = true;
	CollisionSphere->SetCanEverAffectNavigation(false);

	RootComponent = CollisionSphere;

	ProjectileMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Projectile Mesh"));
	ProjectileMesh->SetupAttachment(RootComponent);
	ProjectileMesh->SetCanEverAffectNavigation(false);

	TraceParticle = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Projectile Particle"));
	TraceParticle->SetupAttachment(RootComponent);

	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("Projectile Movement"));
	ProjectileMovement->UpdatedComponent = RootComponent;
	ProjectileMovement->MaxSpeed = 0;

	ProjectileMovement->bRotationFollowsVelocity = true;
	ProjectileMovement->bShouldBounce = ProjectileSettings.IsExplosive;

	if (ProjectileSettings.IsExplosive)
	{
		ProjectileSettings.Lifetime = ProjectileSettings.DetonationTime + 0.1f;

	}

}

// Called when the game starts or when spawned
void AProjectileBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AProjectileBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	ExplosiveTick(DeltaTime);

	if (ProjectileDelta >= ProjectileSettings.Lifetime)
	{
		Destroy();

	}

}

void AProjectileBase::UpdateSettings()
{
	ProjectileMovement->Velocity = RootComponent->GetForwardVector() * ProjectileSettings.Velocity;

}

void AProjectileBase::ExplosiveTick(float DeltaTime)
{
	ProjectileDelta += DeltaTime;

	if (ProjectileSettings.IsExplosive)
	{
		if (ProjectileDelta >= ProjectileSettings.DetonationTime)
		{
			Detonate();

		}

	}
	
}

void AProjectileBase::Detonate()
{
	UGameplayStatics::ApplyRadialDamageWithFalloff(GetWorld(), ProjectileSettings.MaxExplDmg, 1.0f, GetActorLocation(), 0.0f, ProjectileSettings.MaxExplRad, 1.0f, ProjectileSettings.ProjectileDamageType, {}, this);
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ProjectileSettings.Particle, FTransform(FRotator(), GetActorLocation(), FVector(3.0f)));
	UGameplayStatics::SpawnSoundAtLocation(GetWorld(), ProjectileSettings.Sound, GetActorLocation());

	Destroy();

}

void AProjectileBase::CollisionSphereHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector ImpactNormal, const FHitResult& HitRes)
{
	if (!ProjectileSettings.IsExplosive)
	{
		UGameplayStatics::SpawnSoundAtLocation(GetWorld(), ProjectileSettings.Sound, GetActorLocation(), {}, 2.0f);
		UGameplayStatics::SpawnDecalAtLocation(GetWorld(), ProjectileSettings.Decal, FVector(24.0f), HitRes.Location, GetActorRotation());
		Destroy();
	
	}

}

void AProjectileBase::CollisionSphereBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& HitRes)
{
	if (!ProjectileSettings.IsExplosive)
	{
		UGameplayStatics::ApplyDamage(OtherActor, ProjectileSettings.Damage, GetInstigator()->Controller, {}, ProjectileSettings.ProjectileDamageType);
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ProjectileSettings.CharacterParticle, FTransform(GetActorRotation(), GetActorLocation(), FVector(ProjectileSettings.Damage / 3)));

		if (ProjectileSettings.PiercingLvl == 0)
		{
			Destroy();
		}

	}

}

void AProjectileBase::CollisionSphereEndOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (ProjectileSettings.PiercingLvl > 0)
	{
		ProjectileSettings.PiercingLvl -= 1;
	}
	

}

