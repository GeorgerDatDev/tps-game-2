// Fill out your copyright notice in the Description page of Project Settings.


#include "TpsGameInstance.h"

bool UTpsGameInstance::GetWeaponByName(FName WeaponName, FWeaponProperties& OutWeapon)
{
	bool bFound = false;

	FWeaponProperties* NewWeapon = WeaponTable->FindRow<FWeaponProperties>(WeaponName, "", false);

	if (NewWeapon)
	{
		bFound = true;
		OutWeapon = *NewWeapon;

	}
	return bFound;

}

bool UTpsGameInstance::GetAmmoByType(EAmmoType InType, FAmmoProperties& OutAmmo)
{
	bool bFound = false;

	FAmmoProperties* NewAmmo = AmmoTable->FindRow<FAmmoProperties>(UTypes::GetAmmoPureNameFromType(InType), "", false);

	if (NewAmmo)
	{
		bFound = true;
		OutAmmo = *NewAmmo;

	}
	return bFound;

}

