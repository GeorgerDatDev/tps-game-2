// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "Engine/DataTable.h"
#include "FuncLib/Types.h"
#include "TpsGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class TPSGAME_API UTpsGameInstance : public UGameInstance
{
	GENERATED_BODY()

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Tables")
	UDataTable* WeaponTable = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Tables")
	UDataTable* AmmoTable = nullptr;

	UFUNCTION(BlueprintPure)
	bool GetWeaponByName(FName WeaponName, FWeaponProperties& OutWeapon);

	UFUNCTION(BlueprintPure)
	bool GetAmmoByType(EAmmoType InType, FAmmoProperties& OutAmmo);
	
};
