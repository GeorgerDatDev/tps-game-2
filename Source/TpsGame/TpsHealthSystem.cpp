#include "TpsHealthSystem.h"

// Sets default values for this component's properties
UTpsHealthSystem::UTpsHealthSystem()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	SetComponentTickInterval(0.2f);

	// ...
}


// Called when the game starts
void UTpsHealthSystem::BeginPlay()
{
	Super::BeginPlay();

	CurrentHealth = MaxHealth;
	
}


// Called every frame
void UTpsHealthSystem::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UTpsHealthSystem::TakeDamageFlat(float Amount)
{
	CurrentHealth = FMath::Clamp(CurrentHealth - Amount, 0.0f, MaxHealth);

	if (CurrentHealth < 1.0f)
	{
		Die();

	}
	else
	{
		CurrentHealth = trunc(CurrentHealth);

	}

	OnDamageTaken.Broadcast(Amount);
	OnHealthUpdate.Broadcast();

}

void UTpsHealthSystem::Heal(float Amount)
{
	CurrentHealth = FMath::Clamp(CurrentHealth + Amount, 0.0f, MaxHealth);
	CurrentHealth = trunc(CurrentHealth);

	OnHealthUpdate.Broadcast();

}

void UTpsHealthSystem::Die()
{
	bAlive = false;
	OnDeath.Broadcast();

}

