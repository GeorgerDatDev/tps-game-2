#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TpsHealthSystem.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnUpdate);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnFloatPassed, float, Value);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TPSGAME_API UTpsHealthSystem : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UTpsHealthSystem();

	UPROPERTY(BlueprintAssignable)
	FOnUpdate OnHealthUpdate;
	UPROPERTY(BlueprintAssignable)
	FOnUpdate OnDeath;
	UPROPERTY(BlueprintAssignable)
	FOnFloatPassed OnDamageTaken;
	

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UPROPERTY(BlueprintReadOnly, Category = "Vitals")
	bool bAlive = true;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
	float MaxHealth = 50.0f;
	UPROPERTY(BlueprintReadOnly, Category = "Vitals")
	float CurrentHealth = 0.0f;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	float GetCurrentHealth() { return CurrentHealth; }
	float GetMaxHealth() { return MaxHealth; }

	UFUNCTION(BlueprintCallable)
	void TakeDamageFlat(float Amount);
	UFUNCTION(BlueprintCallable)
	virtual void Heal(float Amount);

	virtual void Die();
		
};
