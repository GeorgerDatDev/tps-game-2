#include "TpsPlayerStatSystem.h"
#include "FuncLib/Types.h"
#include "TpsGameInstance.h"
#include <Kismet/GameplayStatics.h>

UTpsPlayerStatSystem::UTpsPlayerStatSystem()
{

}

void UTpsPlayerStatSystem::BeginPlay()
{
	Super::BeginPlay();

	CurrentHealth = MaxHealth;
	CurrentArmor = MaxArmor;
	CurrentStamina = MaxStamina;

	NormalHealthRegen = HealthRegen;

}

void UTpsPlayerStatSystem::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

}

void UTpsPlayerStatSystem::UseStamina(float Amount)
{
	CurrentStamina -= Amount;

}

void UTpsPlayerStatSystem::TakeDamage(float Amount, bool IgnoreArmor)
{
	if (CurrentArmor > 0 && !IgnoreArmor)
	{
		CurrentHealth = FMath::Clamp(CurrentHealth - (Amount / (ArmorEfficiency + 1.0f)), 0.0f, MaxHealth);
		CurrentArmor -= 1;

		OnDamageTaken.Broadcast(Amount / (ArmorEfficiency + 1.0f));

	}
	else
	{
		CurrentHealth = FMath::Clamp(CurrentHealth - Amount, 0.0f, MaxHealth);

		OnDamageTaken.Broadcast(Amount);

	}

	if (CurrentHealth < 1.0f)
	{
		Die();
		CurrentHealth = 0.0f;

	}
	else
	{
		CurrentHealth = trunc(CurrentHealth);

	}

	OnHealthUpdate.Broadcast();

}

void UTpsPlayerStatSystem::RegenerateHealth(float DeltaTime)
{
	static float RegenDelay = 1 / HealthRegen;
	RegenDelay -= DeltaTime;

	if (RegenDelay <= 0)
	{
		if (CurrentHealth < MaxHealth * (HealthRegenThresholdPercent / 100))
		{
			Heal(1.0f);
			RegenDelay = 1 / HealthRegen;
			if (IncreasedHealingLeft > 0)
			{
				IncreasedHealingLeft--;

			}
			else
			{
				HealthRegen = NormalHealthRegen;

			}

		}
		else
		{
			HealthRegen = NormalHealthRegen;
			IncreasedHealingLeft = 0;

		}

	}

}

void UTpsPlayerStatSystem::IncreaseRegeneration(float AddedRegen, int32 DurationInHealth)
{
	HealthRegen += AddedRegen;
	IncreasedHealingLeft += DurationInHealth;

}

void UTpsPlayerStatSystem::ApplySlowness(float Percent, float Duration)
{
	FTimerHandle SlownessTimer;

	PreviousMovementSpeedMultipliers.Add(MovementSpeedMultiplier);
	MovementSpeedMultiplier = FMath::Clamp(MovementSpeedMultiplier - (Percent / 100 * MovementSpeedMultiplier), 0.0f, 1.0f);
	
	OwningCharacter->CharacterUpdate();

	GetWorld()->GetTimerManager().SetTimer(SlownessTimer, this, &UTpsPlayerStatSystem::EndSlowness, Duration, false);
}

bool UTpsPlayerStatSystem::AddArmor()
{
	if (CurrentArmor < MaxArmor)
	{
		CurrentArmor += 2;
		return true;

	}
	else
	{
		OwningCharacter->OnItemConsumtionFailed.Broadcast(FText::FromString("Armor Full"));

	}

	return false;

}

void UTpsPlayerStatSystem::EndSlowness()
{
	MovementSpeedMultiplier = PreviousMovementSpeedMultipliers[PreviousMovementSpeedMultipliers.Num() - 1];
	PreviousMovementSpeedMultipliers.RemoveAt(PreviousMovementSpeedMultipliers.Num() - 1);

	OwningCharacter->CharacterUpdate();

}

void UTpsPlayerStatSystem::Die()
{
	Super::Die();

	if (OwningCharacter)
	{
		OwningCharacter->CurrentState = EPlayerState::Dead_State;
		OwningCharacter->GetMesh()->PlayAnimation(DeathAnim, false);

	}

	FTimerHandle UnposessTimer;
	GetWorld()->GetTimerManager().SetTimer(UnposessTimer, this, &UTpsPlayerStatSystem::UnposessPlayer, 1.0f, false);

}

void UTpsPlayerStatSystem::UnposessPlayer()
{
	if (OwningCharacter->IsPawnControlled())
	{
		OwningCharacter->OnCharDeath.Broadcast();

	}

}
