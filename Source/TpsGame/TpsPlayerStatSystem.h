#pragma once

#include "CoreMinimal.h"
#include "TpsHealthSystem.h"
#include "Character/TpsGameCharacter.h"
#include "TpsPlayerStatSystem.generated.h"

UCLASS( ClassGroup = (Custom), meta = (BlueprintSpawnableComponent) )
class TPSGAME_API UTpsPlayerStatSystem : public UTpsHealthSystem
{
	GENERATED_BODY()

public:

	UTpsPlayerStatSystem();

	ATpsGameCharacter* OwningCharacter = nullptr;

	UPROPERTY(EditAnywhere)
	UAnimationAsset* DeathAnim = nullptr;

protected:

	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MoreStats")
	float HealthRegen = 2.0f;

	/**The percentage of health under which regen is active*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MoreStats")
	float HealthRegenThresholdPercent = 50.0f;

	UPROPERTY(BlueprintReadOnly, Category = "Vitals")
	int32 IncreasedHealingLeft;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
	float MaxArmor = 5.0f;

	/**Divides the health by the Value + 1 when armor is up*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
	float ArmorEfficiency = 1.0f;
	UPROPERTY(BlueprintReadOnly, Category = "Vitals")
	float CurrentArmor = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
	float MaxStamina = 50.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MoreStats")
	float StaminaRegen = 5.0f;
	UPROPERTY(BlueprintReadOnly, Category = "Vitals")
	float CurrentStamina = 0.0f;

	UPROPERTY(BlueprintReadOnly, Category = "Vitals")
	float MovementSpeedMultiplier = 1.0f;
	TArray<float> PreviousMovementSpeedMultipliers;
	//

	float NormalHealthRegen;

	

public:

	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	float GetCurrentStamina() { return CurrentStamina; }
	float GetMaxStamina() { return MaxStamina; }
	float GetMovementSpeed() { return MovementSpeedMultiplier; }
	float GetRegenThreshold() { return HealthRegenThresholdPercent; }

	UFUNCTION(BlueprintCallable)
	void UseStamina(float Amount);

	UFUNCTION(BlueprintCallable)
	void TakeDamage(float Amount, bool IgnoreArmor);

	void RegenerateHealth(float DeltaTime);

	void IncreaseRegeneration(float AddedRegen , int32 Duration);

	bool AddArmor();

	UFUNCTION(BlueprintCallable)
	void ApplySlowness(float Percent, float Duration);

	UFUNCTION()
	void EndSlowness();

	UFUNCTION(BlueprintCallable)
	void Die() override;

	void UnposessPlayer();
	
};
