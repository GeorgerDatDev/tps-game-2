// Fill out your copyright notice in the Description page of Project Settings.


#include "WeaponBase.h"
#include "ProjectileBase.h"
#include "Character/TpsGameCharacter.h"
#include <Kismet/GameplayStatics.h>
#include <Kismet/KismetMathLibrary.h>

// Sets default values
AWeaponBase::AWeaponBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Scene = CreateDefaultSubobject<USceneComponent>("Scene");
	RootComponent = Scene;

	SK_WeaponMesh = CreateDefaultSubobject<USkeletalMeshComponent>("Skeletal Weapon Mesh");
	SK_WeaponMesh->SetGenerateOverlapEvents(false);
	SK_WeaponMesh->SetCollisionProfileName(TEXT("No Collision"));
	SK_WeaponMesh->SetupAttachment(Scene);

	SM_WeaponMesh = CreateDefaultSubobject<UStaticMeshComponent>("Static Weapon Mesh");
	SM_WeaponMesh->SetGenerateOverlapEvents(false);
	SM_WeaponMesh->SetCollisionProfileName(TEXT("No Collision"));
	SM_WeaponMesh->SetupAttachment(Scene);

	MuzzleArrow = CreateDefaultSubobject<UArrowComponent>("Muzzle Arrow");
	MuzzleArrow->SetupAttachment(Scene);

	BulletSleeve = CreateDefaultSubobject<UArrowComponent>("Bullet Sleeve");
	BulletSleeve->SetupAttachment(Scene);

}

// Called when the game starts or when spawned
void AWeaponBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AWeaponBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	SetDispersion(DeltaTime);

}

bool AWeaponBase::CheckMovementState(EPlayerState PlayerMovementState)
{
	return OwningCharacter->CurrentState == PlayerMovementState;

}

void AWeaponBase::FireTick(float DeltaTime)
{
	if (FireTime > 0.0f)
	{
		FireTime -= DeltaTime;

	}
	else if (!ExtraSettings.IsReloading)
	{
		if (IsFiring && ExtraSettings.CurrentAmmo > 0)
		{
			if (!CheckMovementState(EPlayerState::Sprint_State) && !CheckMovementState(EPlayerState::Immobilized_State) && !CheckMovementState(EPlayerState::Dead_State))
			{
				Fire();

			}
			else
			{
				SetIsFiring(false);

			}

		}

	}

}

void AWeaponBase::WeaponInit()
{
	if (SK_WeaponMesh && SK_WeaponMesh->SkeletalMesh)
	{
		SK_WeaponMesh->DestroyComponent(true);

	}

	if (SM_WeaponMesh && SM_WeaponMesh->GetStaticMesh())
	{
		SK_WeaponMesh->DestroyComponent();

	}

}

void AWeaponBase::SetDispersion(float DeltaTime)
{
	WeaponSettings.Dispersion.CurrentDispersion = CheckMovementState(EPlayerState::Aim_State) ? WeaponSettings.Dispersion.InitialDispersion * WeaponSettings.Dispersion.AimMultiplier : WeaponSettings.Dispersion.InitialDispersion;
	WeaponSettings.Dispersion.CurrentDispersion += OwningCharacter->GetVelocity().Size() * (WeaponSettings.Dispersion.MovementCoef * 0.02);

}

FRotator AWeaponBase::GetDispersion(FRotator Direction)
{

	float Yaw = FMath::FRandRange(-WeaponSettings.Dispersion.CurrentDispersion, WeaponSettings.Dispersion.CurrentDispersion);
	float Pitch = FMath::FRandRange(-WeaponSettings.Dispersion.CurrentDispersion, WeaponSettings.Dispersion.CurrentDispersion);

	return Direction + FRotator(Pitch, Yaw, 0.0f);

}

FProjectileProperties AWeaponBase::GetProjectile()
{
	return WeaponSettings.ProjectileSettings;

}

void AWeaponBase::SetIsFiring(bool bFiring)
{
	IsFiring = bFiring;

}

void AWeaponBase::Fire()
{
	FireTime = 1.0f / WeaponSettings.FireRate;

	if (MuzzleArrow)
	{
		FVector SpawnLocation = MuzzleArrow->GetComponentLocation();
		FRotator SpawnRotation;
		
		FHitResult CursorHit;
		UGameplayStatics::GetPlayerController(GetWorld(), 0)->GetHitResultUnderCursor(ECC_GameTraceChannel1, false, CursorHit);

		if (CheckMovementState(EPlayerState::Aim_State))
		{
			SpawnRotation = UKismetMathLibrary::FindLookAtRotation(SpawnLocation, CursorHit.Location + FVector(0, 0, FVector::Dist(SpawnLocation, CursorHit.Location) * 0.2f));

		}
		else
		{
			SpawnRotation = FRotator(GetActorRotation().Pitch, UKismetMathLibrary::FindLookAtRotation(OwningCharacter->GetActorLocation(), CursorHit.Location + FVector(0, 0, FVector::Dist(SpawnLocation, CursorHit.Location) * 0.2f)).Yaw, 0.0f);

		}

		SpawnRotation.Yaw -= 3.0f;
		SpawnRotation = GetDispersion(SpawnRotation);

		FProjectileProperties ProjectileProperties = GetProjectile();
		
		if (ProjectileProperties.Projectile)
		{
			FActorSpawnParameters SpawnParam;
			SpawnParam.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
			SpawnParam.Owner = GetOwner();
			SpawnParam.Instigator = GetInstigator();

			AProjectileBase* DisBullet = Cast<AProjectileBase>(GetWorld()->SpawnActor(ProjectileProperties.Projectile, &SpawnLocation, &SpawnRotation, SpawnParam));

			if (DisBullet)
			{
				DisBullet->ProjectileSettings = ProjectileProperties;
				DisBullet->UpdateSettings();

				ExtraSettings.CurrentAmmo--;

			}

		}
		else
		{
			FHitResult TraceHit;
			GetWorld()->LineTraceSingleByChannel(TraceHit, SpawnLocation, SpawnLocation + UKismetMathLibrary::GetForwardVector(SpawnRotation) * WeaponSettings.TraceDistance, ECC_GameTraceChannel2);

			UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), WeaponSettings.TraceEffect, FTransform(SpawnRotation, SpawnLocation, FVector(0.3f)));

			UGameplayStatics::ApplyDamage(TraceHit.GetActor(), WeaponSettings.TraceDamage, UGameplayStatics::GetPlayerController(GetWorld(), 0), this, WeaponSettings.ProjectileSettings.ProjectileDamageType);

			if (!Cast<APawn>(TraceHit.Actor.Get()))
			{
				UGameplayStatics::SpawnSoundAtLocation(GetWorld(), WeaponSettings.ProjectileSettings.Sound, TraceHit.Location);
				UGameplayStatics::SpawnDecalAtLocation(GetWorld(), WeaponSettings.ProjectileSettings.Decal, FVector(18.0f), TraceHit.Location, SpawnRotation);
			}
			else
			{
				UGameplayStatics::ApplyPointDamage(TraceHit.Actor.Get(), WeaponSettings.TraceDamage, GetActorLocation(), TraceHit, OwningCharacter->Controller, {}, WeaponSettings.ProjectileSettings.ProjectileDamageType);
				UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), WeaponSettings.ProjectileSettings.CharacterParticle, FTransform(TraceHit.Normal.Rotation(), TraceHit.Location, FVector(TraceHit.Distance / WeaponSettings.TraceDistance)));

			}

			ExtraSettings.CurrentAmmo--;

		}

		UGameplayStatics::SpawnSoundAtLocation(GetWorld(), WeaponSettings.FireSound, GetActorLocation(), {}, 1.0f, FMath::FRandRange(0.92, 1.08));
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), WeaponSettings.FireEffect, FTransform(SpawnRotation, SpawnLocation, FVector(0.1f)));
		DropShell(BulletSleeve->GetComponentLocation(), BulletSleeve->GetComponentRotation());
	
	}

}

void AWeaponBase::StartReload()
{
	if (!ExtraSettings.IsReloading && OwningCharacter->GetInventoryComponent()->GetAmmoSlotByType(WeaponSettings.Caliber).Count > 0)
	{
		ExtraSettings.ReloadDelta = WeaponSettings.ReloadTime / OwningCharacter->PlayerSpeedMultiplier;
		ExtraSettings.IsReloading = true;

		if (ExtraSettings.CurrentAmmo > 0)
		{
			UGameplayStatics::SpawnSoundAttached(WeaponSettings.ReloadSoundTactical, RootComponent, {}, {}, EAttachLocation::KeepRelativeOffset, false, 1.0f, OwningCharacter->PlayerSpeedMultiplier);
			OwningCharacter->PlayAnimMontage(WeaponSettings.ReloadAnim, WeaponSettings.AnimTacticalSpeed * OwningCharacter->PlayerSpeedMultiplier);

		}
		else
		{
			UGameplayStatics::SpawnSoundAttached(WeaponSettings.ReloadSoundEmpty, RootComponent, {}, {}, EAttachLocation::KeepRelativeOffset, false, 1.0f, OwningCharacter->PlayerSpeedMultiplier);
			OwningCharacter->PlayAnimMontage(WeaponSettings.ReloadAnim, WeaponSettings.AnimEmptySpeed * OwningCharacter->PlayerSpeedMultiplier);

		}

	}

}

void AWeaponBase::ReloadTick(float DeltaTime)
{
	if (ExtraSettings.IsReloading)
	{
		if (ExtraSettings.ReloadDelta > 0.0f)
		{
			ExtraSettings.ReloadDelta -= DeltaTime;

		}
		else
		{
			Reload();
		}

	}
	else
	{
		if (ExtraSettings.CurrentAmmo <= 0)
		{
			StartReload();

		}

	}

}

void AWeaponBase::Reload()
{
	ExtraSettings.IsReloading = false;

	if (ExtraSettings.CurrentAmmo > 0)
	{
		ExtraSettings.CurrentAmmo = OwningCharacter->GetInventoryComponent()->UseAmmo(WeaponSettings.Caliber, WeaponSettings.MaxAmmo + 1, ExtraSettings.CurrentAmmo);

	}
	else
	{
		ExtraSettings.CurrentAmmo = OwningCharacter->GetInventoryComponent()->UseAmmo(WeaponSettings.Caliber, WeaponSettings.MaxAmmo, ExtraSettings.CurrentAmmo);

	}

	OwningCharacter->GetInventoryComponent()->OnWeaponReload.Broadcast();

}

void AWeaponBase::DropShell(FVector Location, FRotator Rotation)
{
	if (WeaponSettings.BulletShell)
	{
		AActor* Shell = Cast<AActor>(GetWorld()->SpawnActor(WeaponSettings.BulletShell));
		Shell->SetActorLocation(Location);
		Shell->SetActorRotation(Rotation);

	}

}

