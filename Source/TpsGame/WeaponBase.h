// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/ArrowComponent.h"
#include "FuncLib/Types.h"
#include <GameFramework/CharacterMovementComponent.h>
#include "WeaponBase.generated.h"

class ATpsGameCharacter;

UCLASS()
class TPSGAME_API AWeaponBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWeaponBase();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"), Category = Components)
		class USceneComponent* Scene = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"), Category = Components)
		class USkeletalMeshComponent* SK_WeaponMesh = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"), Category = Components)
		class UStaticMeshComponent* SM_WeaponMesh = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"), Category = Components)
		class UArrowComponent* MuzzleArrow = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"), Category = Components)
		class UArrowComponent* BulletSleeve = nullptr;


	UPROPERTY(BlueprintReadWrite)
		FWeaponProperties WeaponSettings;
	UPROPERTY(BlueprintReadOnly)
		FExtraProperties ExtraSettings;
	UPROPERTY()
		ATpsGameCharacter* OwningCharacter = nullptr;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY()
		bool IsFiring = false;

	float FireTime = 0;

	bool CheckMovementState(EPlayerState PlayerMovementState);

	void FireTick(float DeltaTime);

	void WeaponInit();

	void SetDispersion(float DeltaTime);

	UFUNCTION(BlueprintPure)
		FRotator GetDispersion(FRotator Direction);

	FProjectileProperties GetProjectile();

	UFUNCTION()
		void SetIsFiring(bool bFiring);

	void Fire();

	void StartReload();
	void ReloadTick(float DeltaTime);
	void Reload();

	void DropShell(FVector Location, FRotator Rotation);
};
