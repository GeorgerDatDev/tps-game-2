#include "WorldItemBase.h"
#include "TpsGameInstance.h"

// Sets default values
AWorldItemBase::AWorldItemBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	Scene = CreateDefaultSubobject<USceneComponent>("Scene");
	RootComponent = Scene;

	PickupSphere = CreateDefaultSubobject<USphereComponent>(TEXT("Pickup Sphere"));
	PickupSphere->OnComponentBeginOverlap.AddDynamic(this, &AWorldItemBase::SphereBeginOverlap);
	PickupSphere->OnComponentEndOverlap.AddDynamic(this, &AWorldItemBase::SphereEndOverlap);
	PickupSphere->SetSphereRadius(30);

	ItemMesh = CreateDefaultSubobject<UStaticMeshComponent>("Item Mesh");
	ItemMesh->SetGenerateOverlapEvents(false);
	ItemMesh->SetCollisionProfileName(TEXT("No Collision"));
	ItemMesh->SetupAttachment(Scene);

}

// Called when the game starts or when spawned
void AWorldItemBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AWorldItemBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AWorldItemBase::ShowPickupCue(ATpsGameCharacter* Player)
{
	Player->OnPickupReady.Broadcast(FText::FromName(ItemName));

}

void AWorldItemBase::HidePickupCue(ATpsGameCharacter* Player)
{
	Player->OnPickupReady.Broadcast(FText());

}

void AWorldItemBase::PickupItem_Implementation(ATpsGameCharacter* Player)
{
	

}

void AWorldItemBase::SphereBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& HitRes)
{
	ATpsGameCharacter* OverlappedCharacter = Cast<ATpsGameCharacter>(OtherActor);

	if (OverlappedCharacter)
	{
		ShowPickupCue(OverlappedCharacter);

	}

}

void AWorldItemBase::SphereEndOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	ATpsGameCharacter* OverlappedCharacter = Cast<ATpsGameCharacter>(OtherActor);

	if (OverlappedCharacter)
	{
		HidePickupCue(OverlappedCharacter);

	}

}
