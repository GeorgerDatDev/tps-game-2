#pragma once

#include "CoreMinimal.h"
#include "Components/SphereComponent.h"
#include "GameFramework/Actor.h"
#include "Character/TpsGameCharacter.h"
#include "FuncLib/Types.h"
#include "WorldItemBase.generated.h"

UCLASS()
class TPSGAME_API AWorldItemBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWorldItemBase();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"), Category = Components)
		class USceneComponent* Scene = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"), Category = Components)
		USphereComponent* PickupSphere = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"), Category = Components)
		class UStaticMeshComponent* ItemMesh = nullptr;

	//

	UPROPERTY(BlueprintReadWrite)
		FName ItemName{};

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void ShowPickupCue(ATpsGameCharacter* Player);
	void HidePickupCue(ATpsGameCharacter* Player);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
		void PickupItem(ATpsGameCharacter* Player);
		void PickupItem_Implementation(ATpsGameCharacter* Player);

	UFUNCTION()
		void SphereBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& HitRes);
	UFUNCTION()
		void SphereEndOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

};
